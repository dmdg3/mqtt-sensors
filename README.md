# MQTT Sensors Dashboard

## CI/CD Automation

[![pipeline status](https://gitlab.com/dmdg3/mqtt-sensors/badges/master/pipeline.svg)](https://gitlab.com/dmdg3/mqtt-sensors/-/commits/master)
[![pipeline status](https://gitlab.com/dmdg3/mqtt-sensors/badges/develop/pipeline.svg)](https://gitlab.com/dmdg3/mqtt-sensors/-/commits/develop)

## Ports on Master branch
* 5000 - Freire
* 5001 - Ramos
* 5002 - Neto
* 5003 - Gonçalves
* 5004 - Lamy
* 5005 - Coutinho Sá

## Running App

    docker run -dt -p 5003:5000 --rm --name mqttgoncalves marinha1/mqtt-sensors:latest
