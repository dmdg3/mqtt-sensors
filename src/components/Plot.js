import React, { Component } from 'react';
import { ResponsiveContainer, AreaChart, XAxis, YAxis, Tooltip, Area, CartesianGrid, LineChart, Legend, Line } from 'recharts'
import CircularProgress from '@material-ui/core/CircularProgress'
import moment from 'moment';

const API = process.env.REACT_APP_REST_URL

export default class Plot extends Component {

    constructor(props){
        super()
        this.state =  {
            data: null,
             loading: true,
             message:null
            }
 
        setTimeout( () => this.tick(), Math.floor(Math.random() * 100))
    }

    componentDidMount(){
        this.mounted = true // acciona  setSstate no tick
        this.intervalID = setInterval( () => this.tick(), 30000)
    }

    componentWillUnmount(){
        this.mounted = false
        clearInterval(this.intervalID)
    }
   
      tick(){
          let query = API+ 'history?sensor=temperature'
          fetch(query)
          .then(response => response.json())
          .then(result => {if (this.mounted) 
                 this.setState({data: result, loading:false})}
          )
          .catch( error => { if (this.mounted)
               this.setState( {loading:true, message: error} )})
      }
  
      render() {
        const {data} = this.state
        if (this.state.message){
            return <div>#ERR</div>
        }
        if (this.state.loading){
         return   <div><CircularProgress/></div>
            
        }
        //null values
        if (Object.entries(data).length === 0){
 
            return   null
               
           }


           return(
               <div style = {{width:'100%',
               height:'250px'}}>
             <ResponsiveContainer>
                       <AreaChart 
                       margin = {{top: 10}}
                       data = {data}>

                           {/*mostra valores no gráfico*/}
                           <Tooltip></Tooltip>
                       <CartesianGrid strokeDasharray="3 3"></CartesianGrid>
                       <YAxis dataKey="value"></YAxis>
                       <XAxis 
                       /*espaçamento pontos*/
                       minTickGap={30}
                       /*formata a data*/
                       tickFormatter = {(unitTime) => moment(unitTime).format('HH:mm')}
                       dataKey="timestamp"></XAxis>
                       <Area dataKey="value" />
                       </AreaChart>
                
                       </ResponsiveContainer>
               </div>
           )
      }
}