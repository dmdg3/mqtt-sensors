import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

var today = new Date().toLocaleDateString("sv-SE",
{timeZone: "Europe/Lisbon"}).slice(0,10)
export default function DatePickers() {
  const classes = useStyles();

//quando é chamado é call depois, a function compilava logo no onchange
    const handlerChange = (event) => {
            console.log(event.target.value)
            
    }

  return (
    <form className={classes.container} noValidate>
      <TextField
        id="date"
        label="Filter Graph"
        type="date"
        format="dd-MM-yyyy"
        defaultValue={today}
        className={classes.textField}
        onChange={handlerChange}
        InputLabelProps={{
          shrink: true,
        }}
      />
    </form>
  );
}
