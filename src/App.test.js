import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('test for app name', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/mqtt-sensors/i);
  expect(linkElement).toBeInTheDocument();
});
